# 智慧农业云

#### Description
基于SpringCloud的SaaS/PaaS农业物联网平台。核心技术采用Nacos、Fegin、Ribbon、JWT Token、Mybatis、SpringBoot、Redis、RibbitMQ等主要框架和中间件。业务内容包括对农业农技指导、农业采收种植一体化、农产品溯源、仓储配送等核心功能。另外配套开发种植体系中硬件设备的监测、智慧大棚、水肥监控等物联网体系应用。项目希望能够打造农业全链应用系统。目前业务系统开发完成，正在调试嵌入整个SaaS环境

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
