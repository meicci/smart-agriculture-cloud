# 智慧农业云

#### 介绍
基于SpringCloud的SaaS/PaaS农业物联网平台。核心技术采用Nacos、Fegin、Ribbon、JWT Token、Mybatis、SpringBoot、Redis、RibbitMQ等主要框架和中间件。业务内容包括对农业农技指导、农业采收种植一体化、农产品溯源、仓储配送等核心功能。另外配套开发种植体系中硬件设备的监测、智慧大棚、水肥监控等物联网体系应用。项目希望能够打造农业全链应用系统。目前业务系统开发完成，正在调试嵌入整个SaaS环境

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
